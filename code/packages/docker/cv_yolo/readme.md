# Computer-Vision Dockercontainer (cv_yolo)

build with:

~~~text
docker build . -t cv_yolo
~~~

execute with:

~~~text
docker run -it --gpus all -v c/users/marti:/opt/nb -p 8888:8880 cv_yolo /bin/bash -c "mkdir -p /opt/nb && jupyter notebook --notebook-dir=/opt/nb --ip='0.0.0.0' --port=8888 --no-browser --allow-root"
~~~

for GPU support in WSL2, look at this [link](https://dilililabs.com/en/blog/2021/01/26/deploying-docker-with-gpu-support-on-windows-subsystem-for-linux/)
git clone https://github.com/STVIR/pysot.git
cd pysot
CALL conda.bat activate cv-project
python setup.py build_ext --inplace
cd ..
mv pysot\pysot %USERPROFILE%\anaconda3\envs\cv-project\Lib\site-packages
rm -r pysot\pysot
rm -r pysot\build
rm -r pysot\demo
rm -r pysot\testing_dataset
rm -r pysot\toolkit
rm -r pysot\tools
rm -r pysot\training_dataset
rm -r pysot\vot_iter
rm pysot\.gitignore
rm pysot\INSTALL.md
rm pysot\install.sh
rm pysot\LICENSE
rm pysot\MODEL_ZOO.md
rm pysot\README.md
rm pysot\requirements.txt
rm pysot\setup.py
rm pysot\TRAIN.md
pause
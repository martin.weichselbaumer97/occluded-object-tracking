# Occluded Object Detections
## Project Links
 * Google Drive: https://drive.google.com/drive/folders/1w7whu36qgZEYJIGHD1m4QBkf9vU4okTV?usp=sharing
 * GitLab: https://gitlab.com/martin.weichselbaumer97/occluded-object-tracking.git
 * Report: https://www.overleaf.com/2188468864ckvrtxhcbmdc

## External Github Repositories
 * https://github.com/STVIR/pysot
 * https://github.com/open-mmlab/mmtracking
 
## Papers
 * Detection and Tracking of Moving Targets for Thermal Infrared Video Sequences (https://doi.org/10.3390/s18113944)
 * Occlusion and Deformation Handling Visual Tracking for UAV via Attention-Based Mask Generative Network (https://doi.org/10.3390/rs14194756)
 * A Thermal Infrared Video Benchmark for Visual Analysis (https://doi.org/10.1109/CVPRW.2014.39)
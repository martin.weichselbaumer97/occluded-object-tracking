import copy
import datetime
import os
import time

import cv2
import numpy as np
from matplotlib import pyplot as plt

import object_detection.image_processing as ip
import object_detection.object_tracking as ot

# Settings


if os.path.isfile("output.log"):
    os.remove("output.log")

for root, folders, files in os.walk("../tracked_videos"):
    for file in files:  # or
        if file.endswith(".mp4"):
            time0 = time.time()
            
# Computer-Vision Dockercontainer (cv_pytracking_1)

build with:

~~~text
docker build . -t cv_pytracking_1
~~~

execute with:

~~~text
docker run -it -e PORT=8097 -e ENV_PATH=$HOME/.visdom -e LOGGING_LEVEL=INFO -e HOSTNAME=localhost -e BASE_URL=/ -e READONLY=True -e ENABLE_LOGIN=False -e FORCE_NEW_COOKIE=False --gpus all -v c/users/marti:/opt/nb -p 8888:8888 -p 8097:8097 cv_pytracking_1 /bin/bash -c "mkdir -p /opt/nb && (visdom &) && jupyter notebook --notebook-dir=/opt/nb --ip='0.0.0.0' --port=8888 --no-browser --allow-root"
~~~

for GPU support in WSL2, look at this [link](https://dilililabs.com/en/blog/2021/01/26/deploying-docker-with-gpu-support-on-windows-subsystem-for-linux/)
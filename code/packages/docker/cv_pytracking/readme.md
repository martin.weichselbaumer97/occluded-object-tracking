# Computer-Vision Dockercontainer (cv_pytracking)

build with:

~~~text
docker build . -t cv_pytracking
~~~

execute with:

~~~text
docker run -it -e PORT=8097 -e ENV_PATH=$HOME/.visdom -e LOGGING_LEVEL=INFO -e HOSTNAME=localhost -e BASE_URL=/ -e READONLY=True -e ENABLE_LOGIN=False -e FORCE_NEW_COOKIE=False --gpus all -v c/users/marti:/opt/nb -p 8888:8888 -p 8097:8097 cv_pytracking /bin/bash -c "mkdir -p /opt/nb && (visdom &) && jupyter notebook --notebook-dir=/opt/nb --ip='0.0.0.0' --port=8888 --no-browser --allow-root"
~~~

for GPU support in WSL2, look at this [link](https://dilililabs.com/en/blog/2021/01/26/deploying-docker-with-gpu-support-on-windows-subsystem-for-linux/)

cd "/opt/nb/Documents/Computer Vision/occluded-object-tracking/code/models/" && cp "atom_default.pth" "dimp18.pth" "dimp50.pth" "e2e_mask_rcnn_R_50_FPN_1x_converted.pth" "kys.pth" "resnet18_vggmconv1.pth" "siamrpn_alex_dwxcorr.pth" "/workspace/pytracking/pytracking/networks/"
cd /workspace/pytracking/pytracking && python run_video.py dimp dimp18 "/opt/nb/Documents/Computer Vision/occluded-object-tracking/tracked_videos/a.mp4" --debug 1

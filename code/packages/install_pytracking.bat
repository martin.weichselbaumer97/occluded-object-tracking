CALL conda.bat create -y --name pytracking python=3.7
CALL conda.bat activate pytracking
CALL conda.bat install -y pytorch torchvision torchaudio pytorch-cuda=11.7 -c pytorch -c nvidia
CALL conda.bat install -y matplotlib pandas
pip install opencv-python visdom tb-nightly
CALL conda.bat install -y cython
pip install pycocotools
pip install jpeg4py
CALL conda.bat install -y jupyterlab ipykernel jupyter
pause
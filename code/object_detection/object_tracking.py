from __future__ import annotations

import copy

import numpy as np
from sklearn import cluster, metrics
import object_detection.image_processing as ip

class ObjectTracker:
    def __init__(self,
                 frames: "np.ndarray",
                 threshold_intensity: int = 50):
        self.frames = frames.astype("bool")
        self.threshold_intensity = threshold_intensity

        self.stride: float | None = None
        self.length_scale: float | None = None
        self.search_time: int | None = None
        self.maximum_speed: float | None = None
        self.minimum_keypoints: int | None = None

        img_shape = self.frames[0].shape
        self.Xs = [np.array([[xs[i],ys[i]]
                             for xs,ys in [np.where(image>threshold_intensity)]
                             for i in range(len(xs))])
                   for image in frames]
        self.full_image = np.array([[x,y]
                                    for y in range(img_shape[0])
                                    for x in range(img_shape[1])])
        self.object_locations = []
        self.object_indices = []
        self.object_trajectories = []


    def identify_objects(self,
                         stride: int = 1,
                         length_scale: float=1.,
                         verbose=1):
        self.stride = stride
        self.length_scale = length_scale

        img_shape = self.frames[0].shape
        dummy_X = [[-img_shape[0]//2,-img_shape[0]//2],
                   [-img_shape[0]//2-1,-img_shape[0]//2]]
        n_times = len(self.Xs)//stride
        for i in range(n_times):
            if verbose:
                print("\r" * 1000, (i + 1), "of", n_times,
                      end="\n" * int(i == n_times - 1))
            t = i*stride
            if len(self.Xs[t]) >= 2:
                X = np.concatenate([self.Xs[t],dummy_X])*length_scale
                sillhouette_score = -np.infty
                n_clusters = 1
                cluster_centers = None
                while n_clusters<len(X)-1:
                    n_clusters += 1
                    kmeans = cluster.KMeans(n_clusters=n_clusters)
                    y_pred = kmeans.fit_predict(X)
                    new_sillhouette_score = metrics.silhouette_score(X,y_pred)
                    if new_sillhouette_score<sillhouette_score:
                        n_clusters -= 1
                        break
                    sillhouette_score = new_sillhouette_score
                    cluster_centers = kmeans.cluster_centers_/length_scale
                for i,center in enumerate(cluster_centers):
                    if not center[0]<0 and not center[1]<0 and \
                            not center[0]>img_shape[0] and not center[1]>img_shape[1]:
                        self.object_locations.append([t,
                                                      int(np.round(center[0],0)),
                                                      int(np.round(center[1],0))])
                        self.object_indices.append(len(self.object_indices))

    def track_objects(self,
                      search_time: int=10,
                      maximum_speed: float=0.5,
                      minimum_keypoints: int=2):
        self.search_time = search_time
        self.maximum_speed = maximum_speed
        self.minimum_keypoints = minimum_keypoints

        locations_by_id = {i: i for i in range(len(self.object_locations))}
        objects_by_id = [[i] for i in range(len(self.object_locations))]

        ids = []
        scores = []
        connected_ids = []

        for i in range(len(self.object_locations)-1):
            obj_loc1 = self.object_locations[i]
            for j in range(i+1,len(self.object_locations)):
                obj_loc2 = self.object_locations[j]
                dt = obj_loc2[0]-obj_loc1[0]
                if dt>search_time:
                    break
                if not dt==0:
                    distance = (np.sqrt((obj_loc2[1]-obj_loc1[1])**2+
                                       (obj_loc2[2]-obj_loc1[2])**2)+1e-5) * \
                               self.length_scale
                    if distance < maximum_speed*dt:
                        ids.append(i)
                        connected_ids.append(j)
                        scores.append(dt/distance)

            if obj_loc1[0]<self.object_locations[i+1][0] or \
                    i == len(self.object_locations)-2:
                ordered_idxs = np.argsort(scores)[::-1]
                ids = np.array(ids).astype(int)[ordered_idxs]
                connected_ids = np.array(connected_ids).astype(int)[ordered_idxs]
                scores = np.array(scores)[ordered_idxs]
                ids_joined = []
                ids_connected_joined = []
                for score,idx,connected_id in zip(scores,ids,connected_ids):
                    if not idx in ids_joined and not connected_id in ids_connected_joined:
                        objects_by_id[locations_by_id[max([idx,connected_id])]] += copy.deepcopy(
                            objects_by_id[locations_by_id[min([idx,connected_id])]])
                        objects_by_id[locations_by_id[min([idx,connected_id])]] = []
                        for mapped_location in objects_by_id[locations_by_id[max([idx,connected_id])]]:
                            locations_by_id[mapped_location] = locations_by_id[max([idx,connected_id])]
                        ids_joined.append(idx)
                        ids_connected_joined.append(connected_id)
                    if len(ids_joined) == len(ids):
                        break
                ids = []
                scores = []
                connected_ids = []

        self.object_indices = [locations_by_id[idx] for idx in self.object_indices]
        for i in np.unique(self.object_indices):
            locations = np.array([loc
                                  for j,loc in enumerate(self.object_locations)
                                  if self.object_indices[j]==i])
            keypoints = np.array([np.mean(locations[np.where(locations[:,0]==t)],axis=0)
                                  for t in np.unique(locations[:,0])])
            if not keypoints[0,0]==0:
                keypoints = np.concatenate([np.array([[0,keypoints[0,1],keypoints[0,2]]]),keypoints],
                                           axis=0)
            if len(keypoints)>minimum_keypoints:
                trajectory = copy.deepcopy([keypoints[0]])
                trajectory[0][0] = 0
                for t in range(1,len(self.frames)):
                    if len(keypoints)==1:
                        trajectory.append([t,keypoints[0,1],keypoints[0,2]])
                    else:
                        dt = (t-keypoints[0,0])/(keypoints[1,0] - keypoints[0][0])
                        trajectory.append([t,
                                           keypoints[0,1]+(keypoints[1,1]-keypoints[0,1])*dt,
                                           keypoints[0,2]+(keypoints[1,2]-keypoints[0,2])*dt])
                        if t >= keypoints[1][0]:
                            keypoints = np.delete(keypoints,(0,),axis=0)
                self.object_trajectories.append(np.array(trajectory).astype(int))

    def draw(self,
             frames: "np.ndarray",
             smoothening: int = 0,
             box_size: int = 25,
             color: "np.ndarray" | None = None):
        processed_frames = copy.deepcopy(frames)
        if color is None:
            color = np.array([0, 0, 255])
        scaling_x = processed_frames.shape[1]/self.frames.shape[1]
        scaling_y = processed_frames.shape[2]/self.frames.shape[2]
        for trajectory in self.object_trajectories:
            padding = smoothening
            padded_trajectory = np.stack([trajectory[1 + i] for i in range(padding)] +
                                         list(trajectory) +
                                         [trajectory[-1 - i] for i in range(padding)])
            smooth_trajectory = [np.median(padded_trajectory[t:t + 2 * padding + 1], axis=0).astype(int)
                                 for t in range(len(trajectory))]
            for t in range(len(processed_frames)):
                length = box_size
                for i in range(length):
                    for j in range(length):
                        if (i == 0 or i == length - 1) or (j == 0 or j == length - 1):
                            try:
                                processed_frames[t,
                                                 int(smooth_trajectory[t][1] * scaling_x) - length // 2 + i,
                                                 int(smooth_trajectory[t][2] * scaling_y) - length // 2 + j] = color
                            except:
                                print("box out of bounds")
        return processed_frames
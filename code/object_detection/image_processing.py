from __future__ import annotations

import copy
from typing import List, Callable

import cv2
import numpy as np
from matplotlib import pyplot as plt


def read_frames(video_path: str, n_channels: int=None) -> "np.ndarray":
    """
    Function which takes a path to a video file and returns list of
    :param video_path: path to video file
    :return: list of numpy arrays
    """

    cap = cv2.VideoCapture(video_path)
    frames = []
    while True:
        ret, frame = cap.read()
        if ret == False:
            break
        if n_channels is None:
            frames.append(frame)
        else:
            frames.append(frame[:,:,0:n_channels])
    return np.stack(frames)

def write_frames(video_path: str,
                 frames: "np.ndarray",
                 size: tuple | None = None,
                 verbose: bool = False):
    """
    Function which takes a path for a video file and frames and creates the video
    :param video_path: path to video file
    :param frames: numpy array of frames
    :param size: resolution of video
    :param verbose: print progress
    """

    if size is None:
        size = (frames.shape[2],frames.shape[1])
    writer = cv2.VideoWriter(video_path,
    cv2.VideoWriter_fourcc(*"mp4v"), 30, size)
    for i,frame in enumerate(frames):
        if verbose:
            print("\r"*1000,"writing frame",str(i+1),"of",len(frames),
                  end="\n"*int(i==len(frames)-1))
        writer.write(frame.astype('uint8'))
    writer.release()


def show_frame(img: "np.ndarray",
               delay=None):
    """
    Plot image(s).
    :param img: image
    :param delay: delay time [ms] for picture
    """

    if len(img.shape) == 2:
        img = np.expand_dims(img,-1)

    cv2.imshow('display',img)
    cv2.waitKey(delay)
    cv2.destroyAllWindows()


def show_video(imgs: "np.ndarray",
               n_repeat=1,
               delay=None):
    """
    Plot image(s).
    :param imgs: list of image
    :param n_repeat: number of times video is repeated
    :param delay: delay time [ms] for picture
    """
    for i in range(n_repeat):
        if len(imgs.shape) == 3:
            imgs = np.expand_dims(imgs,-1)
        if delay is None:
            delay = 10
        for img in imgs:
            cv2.imshow('display', img)
            cv2.waitKey(delay)
        cv2.destroyAllWindows()


def theta_function(x: "np.ndarray",
                   x0: float,
                   lowpass: bool = True) -> "np.ndarray":
    """
    Theta function.
    :param x: input value
    :param x0: offset
    :param lowpass: if lowpass is applied
    :return:
    """
    y = np.zeros(x.shape)
    if lowpass:
        y[np.where(np.array(x)<x0)] = 1
    else:
        y[np.where(np.abs(x)>x0)] = 1
    return y


def sigmoid_function(x: "np.ndarray",
                     x0: float,
                     lowpass: bool = True) -> "np.ndarray":
    """
    Theta function.
    :param x: input value
    :param x0: offset
    :param lowpass: if lowpass is applied
    :return:
    """
    if lowpass:
        return 1/(1+np.exp((np.abs(x)-x0)))
    return 1/(1+np.exp((x0-np.abs(x))))


def rescale_images(frames: "np.ndarray",
                   factor: float):
    """

    :param frames: series of frames
    :param factor: used to shrink frames for faster processing
    :return:
    """

    size = (frames.shape[2], frames.shape[1])
    return np.stack([cv2.resize(frame, (int(size[0] * factor),
                                        int(size[1] * factor)))
                              for frame in frames])

def renormalize_images(frames):
    frames = frames.astype(float)
    maxv = np.max(frames)
    if not maxv == 0:
        frames = (frames*255.) / maxv
    frames = np.clip(frames,0,255)
    return frames.astype("uint8")


def time_bandpass(frames: "np.ndarray",
                  window: int = 5,
                  stride: int = 1,
                  cutoff_min: float = 0,
                  cutoff_max: float = np.infty,
                  cutoff_min_value: float = 0,
                  cutoff_max_value: float = np.infty,
                  cutoff_function: Callable[["np.ndarray",float,bool], "np.ndarray"] = theta_function,
                  fourier_kernel: "np.ndarray" | None = None,
                  verbose: bool = False) -> "np.ndarray":
    """
    Bandpass filter over time for series of frames.
    :param frames: series of frames
    :param window: number of frames used for Fourier transform
    :param stride: distance between frames
    :param cutoff_min: minimum value in Fourier space
    :param cutoff_max: maximum value in Fourier space
    :param cutoff_function: function for creating cutoff
                            takes in [data (np.ndarray), min/max (float), lowpass (bool)]
    :param verbose: print progress
    :return: filtered frames
    """

    if not fourier_kernel is None:
        assert len(fourier_kernel) == window

    processed_frames = None
    fourier_T = np.fft.fftfreq(window,1)
    fourier_space = np.fft.fftshift(np.expand_dims(np.abs(fourier_T),(1,2)),axes=0)

    n_times = len(frames) // (window * stride)
    if len(frames) % (window * stride):
        n_times += 1

    for i in range(n_times):
        if verbose:
            print("\r" * 1000, (i+1), "of", n_times,
                  end="\n"*int(i==n_times-1))
        offset = 0
        if i == n_times-1:
            offset = (i+1) * window * stride - len(frames)
        frames_to_analyze = frames[(i) * window * stride - offset:
                                   (i + 1) * window * stride - offset:
                                   stride]
        new_frames = np.fft.fft(frames_to_analyze,
                                axis=0)
        new_frames = np.fft.fftshift(new_frames, axes=0)
        # show_video(np.concatenate([renormalize_images(frames_to_analyze),
        #                            renormalize_images(np.abs(new_frames)).astype("uint8")],
        #                           axis=2))
        new_frames = new_frames * cutoff_function(fourier_space,
                                                  cutoff_min,
                                                  False)
        new_frames = new_frames * cutoff_function(fourier_space,
                                                  cutoff_max,
                                                  True)
        new_frames = new_frames * cutoff_function(new_frames,
                                                  cutoff_min_value,
                                                  False)
        new_frames = new_frames * cutoff_function(new_frames,
                                                  cutoff_max_value,
                                                  True)
        if not fourier_kernel is None:
            while len(fourier_kernel.shape) != len(new_frames.shape):
                fourier_kernel = np.expand_dims(fourier_kernel,-1)
            new_frames = new_frames * fourier_kernel
        # show_video(np.concatenate([renormalize_images(frames_to_analyze),
        #                            renormalize_images(np.abs(new_frames)).astype("uint8")],
        #                           axis=2))
        new_frames = np.fft.ifftshift(new_frames, axes=0)
        new_frames = np.fft.ifft(new_frames, axis=0)
        new_frames = np.sqrt(new_frames.real ** 2 + new_frames.imag ** 2)
        maxv = np.max(new_frames)
        if not maxv == 0:
            new_frames = new_frames / np.max(new_frames) * 255
        if i == n_times - 1 and len(frames) % (window * stride):
            new_frames = new_frames[-(((len(frames)-1)//stride)%window+1)::]

        new_frames = np.array(new_frames).astype('uint8')
        if processed_frames is None:
            processed_frames = new_frames
        else:
            processed_frames = np.concatenate([processed_frames,new_frames])
    processed_frames = np.array([processed_frames[i//stride]
                                 for i in range(len(frames))]).astype('uint8')
    return processed_frames

def spatial_bandpass(frames: "np.ndarray",
                     cutoff_min: float = 0,
                     cutoff_max: float = np.infty,
                     cutoff_min_value: float = 0,
                     cutoff_max_value: float = np.infty,
                     cutoff_function: Callable[["np.ndarray",float,bool], "np.ndarray"] = theta_function,
                     fourier_kernel: "np.ndarray" | None = None,
                     t_stride: int = 1,
                     verbose: bool = False) -> "np.ndarray":
    """
    Bandpass filter over time for series of frames.
    :param frames: series of frames
    :param window: number of frames used for Fourier transform
    :param stride: distance between frames
    :param cutoff_min: minimum value in Fourier space
    :param cutoff_max: maximum value in Fourier space
    :param cutoff_function: function for creating cutoff
                            takes in [data (np.ndarray), min/max (float), lowpass (bool)]
    :param verbose: print progress
    :return: filtered frames
    """

    if not fourier_kernel is None:
        assert fourier_kernel.shape[-2] == frames.shape[1] and \
            fourier_kernel.shape[-1] == frames.shape[2]

    processed_frames = np.fft.fft2(frames[::t_stride],axes=[1,2])
    processed_frames = np.fft.fftshift(processed_frames,axes=[1,2])
    # show_video(np.concatenate([renormalize_images(frames[::t_stride]),
    #                            renormalize_images(np.abs(processed_frames)).astype("uint8")],
    #                           axis=2),delay=t_stride*30,n_repeat=100)
    fourier_X,fourier_Y = np.meshgrid(np.fft.fftfreq(processed_frames.shape[2],1),
                                      np.fft.fftfreq(processed_frames.shape[1],1))

    fourier_space = np.fft.fftshift(np.expand_dims(np.sqrt(fourier_X**2+fourier_Y**2),0),
                                    axes=[1,2])

    processed_frames = processed_frames * cutoff_function(fourier_space,
                                              cutoff_min,
                                              False)
    processed_frames = processed_frames * cutoff_function(fourier_space,
                                              cutoff_max,
                                              True)
    processed_frames = processed_frames * cutoff_function(processed_frames,
                                              cutoff_min_value,
                                              False)
    processed_frames = processed_frames * cutoff_function(processed_frames,
                                              cutoff_max_value,
                                              True)
    # show_video(np.concatenate([renormalize_images(frames[::t_stride]),
    #                            renormalize_images(np.abs(processed_frames)).astype("uint8")],
    #                           axis=2),delay=t_stride*30,n_repeat=100)
    if not fourier_kernel is None:
        if len(fourier_kernel.shape) != len(processed_frames.shape):
            fourier_kernel = np.expand_dims(fourier_kernel,0)
            processed_frames = processed_frames * fourier_kernel

    processed_frames = np.fft.ifftshift(processed_frames,axes=[1,2])
    processed_frames = np.fft.ifft2(processed_frames,axes=[1,2])
    processed_frames = np.sqrt(processed_frames.real ** 2 + processed_frames.imag ** 2)
    # maxv = np.max(processed_frames)
    # if not maxv == 0:
    #     processed_frames = processed_frames / np.max(processed_frames) * 255

    processed_frames = np.array([processed_frames[i//t_stride]
                                 for i in range(len(frames))]).astype('uint8')
    return processed_frames


def time_convolution(frames: np.ndarray,
                     kernel: np.ndarray,
                     verbose: bool = False):
    assert kernel.shape[0] % 2
    processed_frames = None
    # frames = frames.astype("int8")

    if len(kernel.shape) == 3:
        padding = kernel.shape[0] // 2
        padded_frames = np.stack([frames[1+i] for i in range(padding)] +
                                 list(frames) +
                                 [frames[-1-i] for i in range(padding)])
        processed_frames = np.stack([np.sum([cv2.filter2D(padded_frames[j+i],
                                                          ddepth=-1,
                                                          kernel=kernel[i])
                                             for i in range(kernel.shape[0])],axis=0)
                                     for j in range(frames.shape[0])])
    if len(kernel.shape) == 1:
        padding = kernel.shape[0] // 2
        padded_frames = np.stack(
            [frames[1 + i] for i in range(padding)] +
            list(frames) +
            [frames[-1 - i] for i in range(padding)])
        processed_frames = np.stack([np.sum([kernel[i]*padded_frames[j+i]
                                             for i in range(kernel.shape[0])],axis=0)
                                     for j in range(frames.shape[0])])
    # processed_frames = np.clip(processed_frames,0,255).astype("uint8")
    return processed_frames


def get_time_kernel_mean_subtraction(n_steps: int,
                                     weights: float | list[float] = 1.):
    length = 2*n_steps+1
    kernel = None
    if type(weights) is float:
        kernel = -weights*np.ones((length,))/length
    if type(weights) is list:
        if len(weights) == length:
            kernel = np.array(weights)
        else:
            kernel = np.array(weights[-1:0:-1]+weights)
    if kernel is None:
        raise ValueError("Kernel could not be constructed")
    kernel[n_steps//2+1] = 1
    return kernel


def get_time_kernel_mean_subtraction_fourier(n_steps: int,
                                             weights: float | list[float] = 1.):
    kernel = get_time_kernel_mean_subtraction(n_steps=n_steps,
                                              weights=weights)
    kernel = np.fft.fft(kernel, axis=0)
    kernel = np.fft.fftshift(kernel, axes=0)
    return kernel